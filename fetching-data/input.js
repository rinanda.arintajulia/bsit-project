const { read } = require('fs')
const fetch = require('node-fetch')

  
async function getData(id){
    try {
        const result = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
        const json = await result.json()
        console.log(json)
        // const hobbies = {
        //     ...json,
        //     hobby: 'Programming'
        // }
        // console.log(hobbies)
        // for(let i=0;i<json.length;i++){
        //     console.log(json[i].name)
        // }
    }catch(err){
        console.log(err.message)
    }
}

async function loopData(){
    const readline = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout,
      });


    readline.question(`Please insert id-> `, id => {
        let res = parseInt(id)
        if (res < 0) {
            console.log("Error, id not valid!")
        }else{
        console.log('This is your data\n',getData(id));
        
    }
    readline.close();
    });
}

// getData(1)
loopData()
// function askForLoop(){
//     var ask = readline("Want to loop?")
//     if (ask == 'y' || 'Y'){
//         loopData()
//     }else{
//         return false
//     }
// }