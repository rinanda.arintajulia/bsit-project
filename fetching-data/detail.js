const fetch = require('node-fetch')

async function getData(){
   try { 
        const result = await fetch('https://jsonplaceholder.typicode.com/users/1')
        const json = await result.json()
        // console.log(json)
        // for(let i=0; i < json.length; i++){
        //     console.log(json[i].name)
        // }
    // }catch(err){
        const json2 = {
            hobby: 'Sleeping',
        }
        const obj3 = {...json, ...json2}
        console.log(obj3)
    } catch (err) {
        console.log(err.message)
    }
}

getData()