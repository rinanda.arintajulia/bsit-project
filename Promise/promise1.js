const wait = new Promise((resolve, reject)=>{
    reject('Error')
    setTimeout(()=>{
        // console.log('After 1 second')
        resolve('After 1 second')
    }, 1000)
})

wait.then((result)=>{
    console.log(result)
}).catch((err)=>{
    console.log(err)
})