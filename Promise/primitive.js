const greet = new Promise((reslove, reject) => {
    reject('Error')
    setTimeout(() => {
      // console.log('After 1 second')
      reslove("After 1 second");
    }, 1000);
  });
  
  async function processAsync() {
    try {
      const result = await greet;
      console.log(result);
    }catch(err){
      console.log(err);
    }
  }
  
  processAsync();